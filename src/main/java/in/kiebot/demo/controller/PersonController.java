package in.kiebot.demo.controller;

import in.kiebot.demo.model.Person;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/api/person")
public class PersonController {
    List<Person> lstPerson= List.of(
            new Person("Anil",20),
            new Person("Arun",22),
            new Person("Mahesh",30)

    );

    // To display data in HTML - Thymeleaf

//    @RequestMapping("")
//    public String showPerson(Model model){
//        Person person=new Person();
//        person.setName("athira");
//        person.setAge(10);
//        model.addAttribute("person",person);
//        return "person";
//    }

//    // To display data in json format {"name":"athira","age":10}
//    @RequestMapping("")
//    @ResponseBody
//    public Person showPerson(){
//        Person person=new Person();
//        person.setName("athira");
//        person.setAge(10);
//        return person;
//    }

//    // To display data in xml format -- download dependency - jackson dataformat xml
//    @RequestMapping(value="/xml",produces= MediaType.APPLICATION_XML_VALUE)
//    @ResponseBody
//    public Person showPersoninXML(){
//        Person person=new Person();
//        person.setName("athira");
//        person.setAge(10);
//        return person;
//    }

//    // To display data in xml format -- download dependency - jackson dataformat xml
//    @RequestMapping(value="/json",produces= MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public Person showPersoninjson(){
//        Person person=new Person();
//        person.setName("athira");
//        person.setAge(10);
//        return person;
//    }

//     To display data in json format -- download dependency - jackson dataformat xml
    @RequestMapping(value="/json",produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Person> showMultiplePersoninjson(){

        return lstPerson;
    }
    //     To display data in xml format -- download dependency - jackson dataformat xml
    @RequestMapping(value="/xml",produces= MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public List<Person> showMultiplePersoninxml(){

        return lstPerson;
    }

}
